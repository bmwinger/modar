# ModMeta

This is the ModMeta specification. It is a work in progress and is not stable. 

The file is a TOML file, following [TOML v1.0.0](https://toml.io/en/v1.0.0).

Note that, while each field in a section can be specified under a section header as shown in the example below, dotted keys are used in the field descriptions for conciseness. 

```toml
id = "example-mod"
name = "Example Mod"
version = "0.1.0"
description = "This is an example mod, for the purpose of this spec."
authors = ["Example Author <name@example.org>"]
license = "CC-0"

docs = ["ModMeta.md"]
readme = "README.md"

keywords = ["tool", "packaging"]

# TODO: Might remove or change. This would be most useful as a hint as to where the next version 
#       can be downloaded (e.g. for sites with predictable URLs), but that is more complicated
[source_urls]
direct = ["https://gitlab.com/bmwinger/modar/-/archive/master/modar-master.zip"]
manual = ["https://gitlab.com/bmwinger/modar/-/releases"]

[urls]
homepages = ["https://gitlab.com/bmwinger/modar"]
docs = ["https://gitlab.com/bmwinger/modar/-/wikis", "https://bmwinger.gitlab.io/modar"]
bugs-to = ["https://gitlab.com/bmwinger/modar/-/issues"]

[dependencies.scheme]
other-mod.min_version = "1.0"

[conflicts.scheme]
third-mod.version = "*"

[[install]]
path="Data Files"

[[install]]
path="Optional"
features=["option"]

[features]
option.desc = "Some optional feature"

```

## Default Section 

### id
```toml
id = "example-mod"
```

Mandatory

A unique and fixed identifier for the mod.

A string consisting of alphanumeric characters, hyphens, underscores and the plus sign.
It must begin with a letter. 

It is recommended that it not end with anything resembling a version, and that [Kebab case](https://en.wikipedia.org/wiki/Kebab_case#Kebab_case) be used.

Regex: `[a-zA-Z][a-zA-Z0-9_+-]*` 

### name
```toml
name = "Example Mod"
```
Mandatory

The name of the mod. No special restrictions are placed on its contents.

It is strongly recommended that the name should be short. 

### description

```toml
description = "This is an example mod, for the purpose of this spec."
```
Mandatory

A description of the mod. No special restrictions are placed on its contents.

It is strongly recommended that the description be restricted to only one sentence. 

### version
```toml
version = "1.0.0"
```
Mandatory

The mod's version. This must conform to [Semantic Versioning](https://semver.org/).

### authors
```toml
authors = ["Example Author <name@example.org>"]
```
Mandatory

A list of author names and email addresses.

### license
```toml
license = "CC0"
```
Optional

The mod's license, as an SPDX license expression. See the [SPDX license list](https://spdx.org/licenses/), for a list of supported licenses and the [SPDX expression spec](https://spdx.dev/spdx-specification-21-web-version/#h.jxpfx0ykyb60) for details of how to specify more complex expressions which combine multiple licenses.

Can be omitted only if a nonempty `LICENSE`, `LICENSE.md` or `LICENSE.txt` file is provided in the same directory as `mod.toml`. License files must be utf-8 encoded plain text.

A custom license should be provided by including it in a `LICENSE` file, and optionally listing its name in the `license` field using a `LicenseRef-` prefix (which is what the SPDX spec requires for licenses not in the SPDX list).

### readme
```toml
readme = "README.md"
```

The path of the readme within the project directory. Can be omitted only if a `README`, `README.md` or `README.txt` file is present in the same directory as `mod.toml`.

The readme file must be utf-8 encoded plain text.

### docs
```toml
docs = ["guide.txt", "docs/*"]
```
Optional

The paths of additional documentation besides the README. These paths may include the `*` wildcard, to match everything within a directory.

### keywords
```toml
keywords = ["example"]
```

A list of keywords to describe the mod. Each keyword can be an arbitrary string. It is strongly recommended that each keyword should be a single word or group of related words.

You should assume that the capitalization of keywords is not significant.

### categories
```toml
categories = []
```

A list of categories that the mod belongs to. Each category must match one of the categories below. It is recommended that a mod belong to no more than 5 categories. 

Categories:
- TODO: FILLME

## source-urls
At least one source URL must be provided, and it is recommended that you include both direct and manual urls.

### direct
```toml
source-urls.direct = ["https://example.org/foo.zip"]
```

A list of URLs which can be used to directly download the mod.

### manual
```toml
source-urls.manual = ["https://example.org/downloads/foo.html"]
```

A list of URLs where the mod can be manually downloaded.

## urls
### homepages
```toml
urls.homepages = ["https://gitlab.com/bmwinger/modar"]
```

A list of homepages for the mod, in order of significance. Each item in the list must be a valid URL.

### docs
```toml
urls.docs = ["https://gitlab.com/bmwinger/modar/-/wikis", "https://bmwinger.gitlab.io/modar"]
```

A list of documentation links. Each item in the list must be a valid URL.

### bugs-to
```toml
bugs-to = ["https://gitlab.com/bmwinger/modar/-/issues"]
```

A list of locations to submit bugs. Each item in the list must either be a valid URL, or an email address prefixed with `mailto:`. 

## dependencies

Dependencies are listed using a third party identification scheme, an identifier, and a name.

Valid schemes include:

- `portmod`: Identifiers are a package name and repository, using portmod's package name scheme. E.g. `assets-meshes/rr-better-meshes::openmw`, referring to [this package](https://portmod.gitlab.io/openmw-mods/assets-meshes/rr-better-meshes/).
- `nexusmods`: Identifiers are a game/id pair, separated by a forward slash. E.g. `morrowind/43266` referring to [this mod](https://www.nexusmods.com/morrowind/mods/43266)
- `github`: Identifiers are a user/group and project pair, separated by a forward-slash. E.g. `Of-Ash-And-Blight/OAAB-Data` for [this mod](https://github.com/Of-Ash-And-Blight/OAAB-Data). 
- `url`: Identifiers are a URL referring to a mod's homepage. To be used when no over scheme is available and should otherwise be avoided since URLs are not a consistent way of identifying a mod.

Identifiers vary depending on the scheme (see above).

A name can optionally be included to identify mods for schemes with numeric identifiers for better readability, and to help identify mods which no longer exist at the source specified by the scheme and identifier.

You can optionally also list supported versions (format TODO, possibly based on [cargo's semver version scheme](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html)).

E.g.

```toml
[dependencies.nexusmods]
"morrowind/43266" = "1.*" 

[dependencies.github]
"Of-Ash-And-Blight/OAAB-Data" = "2.*"

[dependencies.portmod]
"assets-meshes/rr-better-meshes::openmw" = "1.*"
```

TODO: optional dependencies with linked features.
```toml
[dependencies.github]
"Of-Ash-And-Blight/OAAB-Data" = {version="2.*", optional=true}

[features]
oaab = ["dep:github:Of-Ash-And-Blight/OAAB-Data"]
```
The above scheme of referring to dependencies is a little unwieldy.
Maybe dependencies need a simple internal name. E.g.
```toml
[dependencies]
oaab = {scheme="github", id="Of-Ash-And-Blight/OAAB-Data", version="1.*"}

# OR
[dependencies.oaab]
scheme="github"
id="Of-Ash-And-Blight/OAAB-Data"
version="1.*"

[features]
oaab = ["dep:oaab"]
```

## conflicts
TODO (see dependencies)

## install
TODO (see example at top for a WIP example)

## features
TODO (see example at top for a WIP example)
