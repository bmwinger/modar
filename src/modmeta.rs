/*
 *  This file is part of ModAr.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  ModAr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ModAr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ModAr.  If not, see <https://www.gnu.org/licenses/>.
 */

use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use url_serde::SerdeUrl;

// TODO: Engine-specific features
// Plan is to have a target engine field, which enables optional features.
// Certain engines are compatible and share some features, allowing the mods to work on each engine as
// long as they don't use the incompatible features.
//
// I.e. If you declare a mod is written for Morrowind, it will work on OpenMW as long as you don't
// use Morrowind-specific features.
// But you can also declare a mod is written for both Morrowind and OpenMW, in which case both
// feature sets are available, but requirements for one engine will always be unsatisfied when
// working on the other.

//use email_address::EmailAddress;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(tag = "type")]
/// A strictly-enforced requirement that must be satisfied for a feature to be enabled.
/// There may be related dependencies which should be added
/// E.g. for File, the mod which provides the file
pub enum Requirement {
    /// Requirement that a file is present on the filesystem.
    /// This requirement is satisfied if and only if the file exists.
    File { path: String },
    /// A user-specified option which cannot be checked automatically for optional features.
    User { name: String, desc: String },
}

#[derive(Serialize, Deserialize, Debug)]
// Necessary to prevent malformed OptionalData from parsing as RequiredData
// FIXME: We should probably deny unknown fields for everything, since it helps catch mistakes in
// optional fields.
#[serde(deny_unknown_fields)]
pub struct RequiredData {
    /// Path within the archive.
    path: PathBuf,
    /// Output directory within the destination
    rename: Option<PathBuf>,
}

impl RequiredData {
    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn rename(&self) -> Option<&Path> {
        self.rename.as_ref().map(|x| x.as_path())
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OptionalData {
    /// Path within the archive.
    path: PathBuf,
    /// Output directory within the destination
    rename: Option<PathBuf>,
    /// Named option to reference this data
    option: String,
    /// Requirements which must be satisfied if this directory is to be used
    requirements: Vec<Requirement>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    dependencies: HashMap<String, Dependency>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    conflicts: HashMap<String, Dependency>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum Data {
    Optional(OptionalData),
    Required(RequiredData),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Urls {
    homepages: Vec<SerdeUrl>,
    docs: Vec<SerdeUrl>,
    #[serde(rename = "bugs-to")]
    bugs_to: Vec<SerdeUrl>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SourceUrls {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    direct: Vec<SerdeUrl>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    manual: Vec<SerdeUrl>,
}

#[derive(Serialize, Deserialize, Debug, Hash, PartialEq, Eq)]
pub enum Category {
    // TODO: Determine a standard set of categories
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct ModMeta {
    // TODO: Localised strings
    /// The name of the mod
    id: String,
    /// The name of the mod
    name: String,
    /// The description of the mod
    description: String,
    /// The mod's version
    // FIXME: What about versions from version-control tags?
    version: semver::Version,
    // FIXME: Should be "Name <email@domain>". Slightly tricky to handle
    authors: Vec<String>,
    // FIXME: Should refer to a specific license, or maybe point to an URL
    /// The mod's license, as an SPDX license expression.
    ///
    /// See `<https://spdx.org/licenses/>`_ for a list of supported licenses
    ///
    /// And `<https://spdx.dev/spdx-specification-21-web-version/#h.jxpfx0ykyb60>`
    /// for the details of how to specify more complex expressions.
    ///     E.g. ``MIT OR GPL-3+``
    ///
    /// Can be omitted only if a nonempty LICENSE file is provided
    // TODO: Custom license file paths (e.g. md/txt extensions)
    // TODO: This doesn't cover the Wrye modding licenses, however those are informal
    //  https://wryemusings.com/WML%201.0.html
    // No formal modding licenses are known, so it's recommended to just use Creative Commons
    // or software liceses.
    // Also see https://en.uesp.net/wiki/Morrowind_Mod:Licensing
    // Note that SPDX allows custom licenses via LicenseRef-{name}
    // It should be possible to restrict LicenseRefs to a custom list
    /// License files must be plain text, and may be parsed as markdown
    license: Option<String>,
    urls: Urls,
    source_urls: SourceUrls,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    /// Supports globbed paths
    // TODO: Support globbed paths!
    docs: Vec<PathBuf>,
    /// The path of the readme file
    /// If omitted, either README, README.txt or README.md must exist
    /// Readmes must be plain text, and may be parsed as markdown.
    readme: Option<String>,
    /// For reference only. Not enforceable due to various reasons including unknown version
    /// schemes of other mods
    /// This mod should only be installed if all dependencies are satisfied
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    dependencies: HashMap<String, Dependency>,
    /// This mod should not be installed if any of the conflicts are satisfied
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    conflicts: HashMap<String, Dependency>,
    #[serde(default, skip_serializing_if = "HashSet::is_empty")]
    /// A list of keywords to describe this mod
    keywords: HashSet<String>,
    #[serde(default, skip_serializing_if = "HashSet::is_empty")]
    /// A list of categories this mod belongs to
    categories: HashSet<Category>,
    // TODO:
    // data: Vec<Data>,
}

impl ModMeta {
    /*
    pub fn data(&self) -> &Vec<Data> {
        &self.data
    }
    */
}

// TODO: Needs validation since version should only be used if neither min nor max is used
// min and max can be used together.
#[derive(Serialize, Deserialize, Debug, PartialEq)]
/// For reference only. Not enforceable due to various reasons including unknown version
/// schemes of other mods
/// For strictly-enforcable rules, see Requirements
// TODO: Optional dependencies related to optional install data
// Note that most more complex version comparisons are difficult since there is no
// enforced version scheme for dependencies.
pub struct Dependency {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    version: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    min_version: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    max_version: Option<String>,
}

pub fn check_doc(path: &Path, name: &str) -> Result<String> {
    for entry in std::fs::read_dir(path)? {
        let entry = entry?;
        if entry.file_name() == name {
            return Ok(entry.file_name().into_string().unwrap());
        }
        if let Some(ext) = entry.path().extension() {
            if ext == "md" || ext == "txt" {
                return Ok(entry.file_name().into_string().unwrap());
            }
        }
    }

    bail!("File does not exist!")
}

pub fn check_nonempty(path: &Path) -> Result<()> {
    if std::fs::File::open(path)?.metadata()?.len() == 0 {
        bail!("File is empty!");
    }
    Ok(())
}

pub fn parse(path: &Path) -> Result<ModMeta> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let meta: ModMeta = toml::from_str(&contents)?;

    let parent_dir = path.parent().unwrap_or(Path::new("."));

    if let Some(license) = &meta.license {
        spdx::Expression::parse(license)?;
    } else {
        let license = check_doc(parent_dir, "LICENSE").context(concat!(
            "You must choose a license for your mod ",
            "by either providing a LICENSE, LICENSE.md or LICENSE.txt file ",
            "or specifying the name of a license in the license field"
        ))?;
        check_nonempty(&parent_dir.join(license))?;
    }

    if let Some(readme) = &meta.readme {
        if !parent_dir.join(readme).exists() {
            bail!(
                concat!(
                    "The readme file {} does not exist. ",
                    "It must be a path relative to the directory containing the mod.toml file"
                ),
                readme
            )
        }
    }
    let readme = check_doc(parent_dir, "README").context(concat!(
        "You must provide a README for your mod ",
        "by either providing a README, README.md or README.txt file ",
        "or specifying the path of a file in the readme field"
    ))?;
    check_nonempty(&parent_dir.join(readme))?;

    // TODO: Check that glob patterns in doc are valid patterns

    // TODO: Validate the id field
    Ok(meta)
}
