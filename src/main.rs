/*
 *  This file is part of ModAr.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  ModAr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ModAr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ModAr.  If not, see <https://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate clap;
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate log;

mod modmeta;

pub use crate::modmeta::ModMeta;
use anyhow::{Context, Result};
use clap::Parser;
use std::path::Path;
use walkdir::WalkDir;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Sets the level of verbosity, which is higher the more times this argument is repeated.
    #[clap(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    /// Run in quiet mode. Takes priority over --verbose
    #[clap(short, long)]
    quiet: bool,

    /// Path of the mod to check.
    #[clap(value_parser, value_name = "MOD_DIR")]
    path: String,

    #[clap(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// Checks the mod's structure
    Check,
    /*
    /// Installs the mod
    Install {
        /// Directory to install into
        #[clap(value_parser, value_name = "OUT_DIR")]
        output_dir: String,
        /// Path of the mod to install.
        #[clap(value_parser, value_name = "MOD_DIR", default_value_t = String::from("."))]
        path: String,
    },
    */
}

/*
fn install_dir(source_path: &Path, dest_path: &Path) -> Result<()> {
    info!("Creating directory {}", dest_path.display());
    std::fs::create_dir_all(&dest_path)
        .context(format!("creating directory {}", dest_path.display()))?;

    for entry in WalkDir::new(&source_path) {
        let entry = entry?;
        // FIXME: Find path relative to destination, create parent directories, then copy file
        debug!("Installing path {}", entry.path().display());
        let file_type = entry.file_type();
        // Both symlinks and files get copied, but directories get created as-needed
        if !file_type.is_dir() {
            if file_type.is_symlink() {
                // TODO: Create destination symlink
                unimplemented!()
            } else {
                let parent = &entry.path().parent().unwrap();
                std::fs::create_dir_all(parent)
                    .context(format!("creating directory {}", parent.display()))?;
                let dest_file_path = dest_path.join(entry.path().strip_prefix(source_path)?);
                debug!("Copying path {} to {}", entry.path().display(), dest_file_path.display());
                std::fs::copy(entry.path(), &dest_file_path).context(format!(
                    "copying file {} to {}",
                    entry.path().display(),
                    dest_file_path.display()
                ))?;
            }
        }
    }
    Ok(())
}

fn install(meta: ModMeta, mod_dir: &Path, output_dir: &Path) -> Result<()> {
    use crate::modmeta::Data;

    for datadir in meta.data() {
        match datadir {
            Data::Required(datadir) => {
                let output_dir = datadir
                    .rename()
                    .map(|x| output_dir.join(x))
                    .unwrap_or(output_dir.to_path_buf());
                let source_dir = mod_dir.join(datadir.path());
                install_dir(&source_dir, &output_dir)
                    .context(format!("installing directory {}", source_dir.display()))?;
            }
            Data::Optional(datadir) => {
                unimplemented!()
            }
        }
    }
    Ok(())
}
*/

fn main() -> Result<()> {
    let args = Cli::parse();

    let verbosity = if args.quiet { 0 } else { 1 + args.verbose };

    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity as usize)
        .init()
        .unwrap();

    match args.command {
        Some(Commands::Check) => {
            let meta = modmeta::parse(&Path::new(&args.path).join("mod.toml"))
                .context("Unable to parse mod.toml")?;
            trace!("Parsed metadata: {:?}", meta);
        }
        /*Some(Commands::Install { path, output_dir }) => {
            let meta = modmeta::parse(&Path::new(&path).join("mod.toml"))
                .context("Unable to parse mod.toml")?;
            install(meta, &Path::new(&path), &Path::new(&output_dir))?
        }*/
        None => {
            use clap::CommandFactory;
            let mut cmd = Cli::command();
            cmd.print_help()?;
        }
    }

    Ok(())
}
