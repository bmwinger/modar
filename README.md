# ModAr

This is a WIP implementation of a new metadata format for mods (modmeta). The CLI tool currently only validates the format, but the idea is that it will also provide installation information and a CLI install tool.

This is primarily targetting the Morrowind and OpenMW modding communities, but ideally it could be written in such a way that it can work with mods for other games.

See [ModMeta.md](ModMeta.md) for the specification. 

The names used (ModAr, ModMeta) are working titles and may change if better names are found.
